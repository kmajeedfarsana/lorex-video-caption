import boto3
import srt
import json
import os
import logging

from datetime import timedelta
from botocore.exceptions import ClientError

class GenerateCaptionsForVideoHandler:
    def __init__(self):
        self.s3 = boto3.resource("s3")
        self.s3_client = boto3.client('s3')
        self.lambda_client = boto3.client('lambda')
        self.logger = logging.getLogger()
        self.logger.setLevel(os.environ['LOG_LEVEL'].upper())
        stage = os.environ['STAGE']
        with open('./config/config-'+stage+'.json') as json_data:
               self.config_json = json.load(json_data)

    def handle(self, records):
        results = {}
        for record in records:
            if(self.logger.isEnabledFor(logging.DEBUG)):
                self.logger.debug(record)
            event_name = record['eventName']
            if 'MODIFY' in event_name:
              s3_key = record['dynamodb']['Keys']['S3Key']['S']
              s3_bucket = record['dynamodb']['Keys']['S3Bucket']['S']
              va_complete_count = int(record['dynamodb']['NewImage']['VAComplete']['N'])
              if(self.logger.isEnabledFor(logging.DEBUG)):
                self.logger.debug("Bucket:" + s3_bucket)
                self.logger.debug("Video:" + s3_key)
                self.logger.debug("Count:" + str(va_complete_count))
              try:
                s3_srt_path = s3_key.replace('.mp4', '.srt')
                self.s3.Object(s3_bucket, s3_srt_path).load()
              except ClientError as e:
                if e.response['Error']['Code'] == "404":
                   if (va_complete_count >= 3):
                     results = self.generate_srt_file(s3_key, s3_bucket, record)
        return results
    
    def generate_srt_file(self, s3_key, s3_bucket, record):
        status = False
        
        label_index = 0
        subs = []
        
        min_confidence = self.config_json['confidence']
        has_labels =True
        has_persons = True
        
        try:
            labels = record['dynamodb']['NewImage']['Labels']['S']
            labels_json = json.loads(labels.replace('\'', '"'))
        except KeyError as e:
            has_labels = False
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug("No labels found.")
            
        try:
            persons = record['dynamodb']['NewImage']['Persons']['S']
            persons_json = json.loads(persons.replace('\'', '"'))
        except KeyError as e:
            has_persons = False
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug("No persons found.")
        
        last_index_time = timedelta(seconds=0)
        
        if has_labels:
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug("Processing labels..") 
            for label in labels_json:
                time_in_seconds = label['Timestamp']/ 1000.0
                name = label['Label']['Name']
                confidence = label['Label']['Confidence']
                td_start = timedelta(seconds=time_in_seconds)
                td_end = timedelta(seconds=time_in_seconds+1)
                if(label_index == 0 or td_start > last_index_time):
                    if(confidence > min_confidence):
                        label_index+=1
                        subs.insert(label_index-1, srt.Subtitle(index=label_index, start=td_start, end=td_end, content=name))
                        last_index_time = td_start
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug(subs)
            subs = list(srt.sort_and_reindex(subs))
            
        subtitle_length = len(subs)
        if(self.logger.isEnabledFor(logging.DEBUG)):
          self.logger.debug('Length:' + str (subtitle_length))
        person_index = subtitle_length
        last_index_time = timedelta(seconds=0)
        label_index = 0
        if has_persons:
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug("Processing persons..")
            for person in persons_json:
                time_in_seconds = person['Timestamp']/ 1000.0
                people_index = person['Person']['Index']
                try:
                    confidence = person['Person']['Face']['Confidence']
                except KeyError as e:
                    continue
                td_start = timedelta(seconds=time_in_seconds)
                td_end = timedelta(seconds=time_in_seconds+1)
                if(person_index == 0 or td_start > last_index_time):
                    added = False
                    if(confidence > min_confidence):
                        for label_index in range(0,subtitle_length):
                            time_stamp = subs[label_index].start
                            if(td_start == time_stamp):
                                content = subs[label_index].content
                                content = content + ', ' + 'Person' + str(people_index)
                                subs[label_index].content = content
                                added = True
                                break
                            elif (td_start < time_stamp):
                                content = 'Person' + str(people_index)
                                subs.insert(person_index, srt.Subtitle(index=person_index+1, start=td_start, end=td_end, content=content))
                                person_index += 1
                                added = True
                                break
                        if not added:
                             content = 'Person' + str(people_index)
                             subs.insert(person_index, srt.Subtitle(index=person_index+1, start=td_start, end=td_end, content=content))
                             person_index += 1
                        last_index_time = td_start
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug(subs)
            subs = list(srt.sort_and_reindex(subs))
            
        if(len(subs) > 0):
            for index in range(0,len(subs)-1):
                if(subs[index].end > subs[index+1].start ):
                    subs[index].end = subs[index+1].start
                    
            subtitle = srt.compose(subs)
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug(srt.compose(subs))
            
            s3_path = s3_key.replace('.mp4', '.srt')
            s3_object = self.s3.Bucket(s3_bucket).put_object(ACL='authenticated-read', Key=s3_path, Body=subtitle)
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug(s3_object)
            key_split = s3_key.split('/')
            folder_path ='';
            for idx, val in enumerate(key_split):
                if(idx+1 < len(key_split)):
                    folder_path = folder_path + '/' + val
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug('Folder path:' + folder_path)
            s3_destination_url = 's3://' + s3_bucket + folder_path + '/'
            s3_video_url = 's3://' + s3_bucket + '/' + s3_key
            s3_srt_url = 's3://' + s3_bucket + '/' + s3_path
            
            data = {}
            data['namemodifier'] = '-captioned'
            data['destination'] = s3_destination_url
            data['source-srt-file'] = s3_srt_url
            data['video-file-input'] = s3_video_url
            payload = json.dumps(data)
            if(self.logger.isEnabledFor(logging.DEBUG)):
              self.logger.debug(payload)
            
            function_name = self.config_json['mediaConvertLambda']
            response = self.lambda_client.invoke(
                                     FunctionName=function_name,
                                     InvocationType='Event',
                                     LogType='Tail',
                                     Payload= payload
                                     )
            
            status = response['Payload'].read().decode()
        return status