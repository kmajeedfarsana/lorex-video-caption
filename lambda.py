from main.generate_captions_for_video_handler import GenerateCaptionsForVideoHandler


def handler(event, context):
    records = event['Records']
    lambda_handler = GenerateCaptionsForVideoHandler()
    return lambda_handler.handle(records)
